<?php



namespace App\Http\Controllers;
use App\Project;
use App\Requests;
use Illuminate\Http\Request;
use Carbon\Carbon;
class ProjectController extends Controller
{

    public function index(){
        //
    }
    public function create()
    {
        return view('naujas');
    }
    public function store(Request $request)
    {
        $this->validate($request, array(

            'title' => 'required|max:255',
            'body' => 'required'
        ));
            $project = new Project;
            $project->title = $request->title;
            $project->body = $request->body;
            $project->created_at = carbon::now();
            $project->updated_at = carbon::now();
            $project->save();
            return redirect()->route('projects.show', $project->id);

    }
    public function show($id){


    }
}
