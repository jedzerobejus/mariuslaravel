@extends('parent',['meta_title'=>'KVK radijas'])

@section('content')

    <!-- Heading Row -->
    <div class="row">
        <div class="col-md-8">
            <img class="img-responsive img-rounded" src="{{asset('img/mic.jpg')}}" alt="">
        </div>
        <!-- /.col-md-8 -->
        <div class="col-md-4">
            <h1>Pagrindinė interneto svetainės naujiena</h1>
            <p> Čia talpinamas sutrumpintas tekstas prieš naujieną</p>
            <a class="btn btn-primary btn-lg" href="http://s45.myradiostream.com:12690/played.html">Dabar groja</a>
        </div>
        <!-- /.col-md-4 -->
    </div>
    <!-- /.row -->

    <hr>

@stop