@extends('parent',['meta_title'=>'KVK radijas'])

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h1>Naujo projekto sukūrimas</h1>
        <hr>
        <form method="POST" action="{{route('createProject')}}">
            <div class="form-group">
                <label name="title">Pavadinimas:</label>
                <input id="title" name="title" class="form-control">
            </div>
            <div class="form-group">
                <label name="body">Projektas:</label>
                <textarea id="body" name="body" rows="10" class="form-control"></textarea>
            </div>
            <input type="submit" value="Projekto sukūrimas" class="btn btn-success btn-lg btn-block">
            <input type="hidden" name="_token" value="{{ Session::token() }}">
        </form>
    </div>
</div>﻿
@stop