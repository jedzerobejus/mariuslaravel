@extends('parent',['meta_title'=>'KVK radijas'])

@section('content')
<div class="container">
<div id="signupbox" style="margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Registracija</div>
            <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="{{ route('prisijungimas')}}" onclick="$('#signupbox').hide(); $('#loginbox').show()">Prisijungimas</a></div>
        </div>
        <div class="panel-body" >
            <form id="signupform" class="form-horizontal" role="form">

                <div id="signupalert" style="display:none" class="alert alert-danger">
                    <p>Error:</p>
                    <span></span>
                </div>


                <div class="form-group">
                    <label for="firstname" class="col-md-3 control-label">Prisijungimo vardas</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="Prisijungimo vardas" placeholder="Prisijungimo vardas">
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastname" class="col-md-3 control-label">Elektroninis paštas</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="lastname" placeholder="el. paštas">
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-md-3 control-label">Slaptažodis</label>
                    <div class="col-md-9">
                        <input type="password" class="form-control" name="passwd" placeholder="slaptažodis">
                    </div>
                </div>


                <div class="form-group">
                    <!-- Button -->
                    <div class="col-md-offset-3 col-md-9">
                        <button id="btn-signup" type="button" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Registruotis</button>
                    </div>
                </div>

                <div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">

                    <div class="col-md-offset-3 col-md-9">

                    </div>

                </div>
            </form>
        </div>
    </div>
</div>
</div>
@stop