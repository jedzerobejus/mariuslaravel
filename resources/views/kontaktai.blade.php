@extends('parent',['meta_title'=>'KVK radijas'])

@section('content')
<div class="col-sm-8 text-left">
    <h3>Kontaktai</h3>
    <p>Čia yra kontaktų skiltis</p>
    <hr>
    <h4>Mūsų kontaktai:</h4>
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th>Vardas Pavardė</th>
                <th>Elektroninis paštas</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>Vardenis P.</td>
                <td>Vardenis@gmail.com</td>
            </tr>
            <tr>
                <td>Marius K.</td>
                <td>Kolozinskas.Marius@gmail.com</td>
            </tr>
            </tbody>
        </table>
    </div>

</div>
<!-- /.row -->

<hr>
@stop