<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('',[
    function(){return View::make('home');},
    'as'=>'home'
]);
Route::get('/apie',[
    function(){return View::make('apie');},
    'as'=>'apie'
]);
Route::get('/projektai',[
    function(){return View::make('projektai');},
    'as'=>'projektai'
]);
Route::get('/kontaktai',[
    function(){return View::make('kontaktai');},
    'as'=>'kontaktai'
]);
Route::get('/prisijungimas',[
    function(){return View::make('prisijungimas');},
    'as'=>'prisijungimas'
]);
Route::get('/registracija',[
    function(){return View::make('registracija');},
    'as'=>'registracija'
]);
Route::post('/naujas',[
    function(){return View::make('naujas');},
    'uses'=>'ProductController@store',
    'as'=>'createProject'
]);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
