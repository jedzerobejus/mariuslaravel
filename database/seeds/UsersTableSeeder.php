<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('admins')->insert([

            'email'=>'admin@gmail.com',
            'name'=>'admin',
            'password'=>bcrypt('admin'),
            'roles'=>2,


        ]);
        Model::reguard();
        $this->command->info('Admin added!');
    }
}
